# Sentiment Analysis Project

## Overview

This project analyzes sentiments expressed in tweets using machine learning techniques. It leverages the Sentiment140 dataset from Kaggle, using a neural network model built with TensorFlow and Keras. This README details the steps required to set up and run the project, from data download to deployment with Flask.

## Prerequisites

- Python 3.8+
- Kaggle API credentials
- Flask for API deployment
- TensorFlow 2.x
- NLTK for natural language processing

## Installation

### Setting up a Virtual Environment

First, ensure you have Python installed. Create and activate a virtual environment:

```bash
python -m venv venv
source venv/bin/activate  # On Windows use `venv\Scripts\activate`
```

### Installing Dependencies

Install the required packages specified in `requirements.txt`:

```bash
pip install -r requirements.txt
```

### Kaggle API Setup

Ensure your Kaggle API credentials (`kaggle.json`) are placed in the appropriate location (`~/.kaggle/kaggle.json` on Unix-like OSes).

## Downloading the Data

The data can be downloaded by running the provided `download_data` function which uses the Kaggle API:

```python
from src.data import download_data
download_data()
```

## Running the Project

To run the project, execute the main script:

```bash
python -m src.main
```

## Project Structure

- `src/`: Source code including data loading, preprocessing, and model operations.
  - `data.py`: Data downloading and loading functions.
  - `preprocessing.py`: Data cleaning and preprocessing functions.
  - `model.py`: Model training and evaluation functions.
- `models/`: Saved models and tokenizer configurations.
- `data/`: Raw and processed data.
- `requirements.txt`: Project dependencies.

## Flask API Deployment

After training the model, you can start the Flask app to serve predictions:

```bash
python app.py
```

This starts a web server that hosts an API for sentiment prediction, accessible at `http://localhost:5000`. You can send POST requests to `/predict` to get sentiment predictions.

## License

This project is licensed under the Apache License 2.0.
