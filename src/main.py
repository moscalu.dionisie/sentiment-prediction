import nltk
from sklearn.model_selection import train_test_split
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from src.data import load_data, download_data
from src.preprocessing import preprocess_data
from .visualization import plot_training_history
from src.model import evaluate_model, train_and_load_model
import json

def download_nltk_resources():
    """Download necessary NLTK resources if they aren't already downloaded."""
    resources = ['stopwords', 'punkt']
    for resource in resources:
        nltk.download(resource, quiet=True)
        
def save_tokenizer(tokenizer, path='models/tokenizer.json'):
    """Save the tokenizer configuration to a JSON file."""
    tokenizer_json = tokenizer.to_json()
    with open(path, 'w', encoding='utf-8') as f:
        f.write(json.dumps(tokenizer_json, ensure_ascii=False))
    print(f"Tokenizer saved to {path}")

def main():
    print("Downloading NLTK resources...")
    download_nltk_resources()
    
    print("Downloading and unzipping the dataset...")
    download_data()
    
    print("Loading data...")
    df = load_data('./data/raw/training.1600000.processed.noemoticon.csv')
    print(f"Data loaded: {df.shape[0]} rows.")
    
    vocab_size = 10000   
    tokenizer = Tokenizer(num_words=vocab_size)
    tokenizer.fit_on_texts(df['text'])

    print("Preprocessing data...")
    processed_data_path = './data/processed/processed_tweets.csv'
    df = preprocess_data(df, 'text', processed_data_path, tokenizer, max_length=100)
    print("Data preprocessing complete.")

    # Save tokenizer configuration to JSON
    save_tokenizer(tokenizer)

    print("Building model...")
    X_train, X_test, y_train, y_test = train_test_split(df['tokens'], df['target'], test_size=0.3, random_state=42)
    X_train = np.array(X_train.tolist())
    y_train = np.array(y_train)
    X_test = np.array(X_test.tolist())
    y_test = np.array(y_test)
    
    print("Training model...")
    # Load the best model saved during training
    history, best_model = train_and_load_model(
        X_train, y_train, X_test, y_test, vocab_size
    )

    # Continue with evaluation if the model is successfully loaded
    if best_model:
        print("Evaluating model...")
        test_results = evaluate_model(best_model, X_test, y_test)
        print(f"Test Accuracy: {test_results[1]*100:.2f}%")
        print("Visualizing training history...")
        plot_training_history(history)
    else:
        print("Failed to proceed with evaluation due to loading issues.")

    print("Visualizing training history...")
    plot_training_history(history)

if __name__ == "__main__":
    main()
