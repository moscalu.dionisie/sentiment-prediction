import re
import pandas as pd
import os
from tensorflow.keras.preprocessing.sequence import pad_sequences
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import string

# Precompile regex patterns
mention_pattern = re.compile(r'@\w+')
url_pattern = re.compile(r'http\S+')
number_pattern = re.compile(r'\d+')
space_pattern = re.compile(r'\s+')

def clean_text(text):
    """Clean text by removing unnecessary characters and altering the format of words."""
    text = mention_pattern.sub('', text)
    text = url_pattern.sub('', text)
    text = number_pattern.sub('', text)
    text = text.lower()
    text = space_pattern.sub(' ', text).strip()
    return text

def tokenize(text):
    """Tokenize text into words after removing stopwords and punctuation."""
    stop_words = set(stopwords.words('english') + list(string.punctuation))
    words = word_tokenize(text)
    return [word for word in words if word not in stop_words]


def preprocess_data(df, text_column, output_file, tokenizer, max_length=100):
    batch_size = 10000
    total_rows = len(df)
    
    
    if 'cleaned_text' not in df.columns:
        df['cleaned_text'] = pd.Series(index=df.index, dtype=object)
    if 'tokens' not in df.columns:
        df['tokens'] = pd.Series(index=df.index, dtype=object)
    
    for start in range(0, total_rows, batch_size):
        end = min(start + batch_size, total_rows)
        print(f"Processing batch from row {start} to {end}...")
        batch_texts = df.loc[start:end-1, text_column].apply(clean_text)  
        df.loc[start:end-1, 'cleaned_text'] = batch_texts
        sequences = tokenizer.texts_to_sequences(batch_texts)
        padded_sequences = pad_sequences(sequences, maxlen=max_length, padding='post')
        
 
        tokens_list = [list(seq) for seq in padded_sequences]
        
 
        df.loc[start:end-1, 'tokens'] = pd.Series(tokens_list, index=df.index[start:end])
    
    # Ensure the output directory exists
    os.makedirs(os.path.dirname(output_file), exist_ok=True)
    
    # Save the processed DataFrame to a CSV file
    df.to_csv(output_file, index=False)
    print(f"Processed data saved to {output_file}")

    return df


