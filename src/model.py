import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Embedding, LSTM, Dense, Dropout
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.optimizers import Adam

def create_and_save_tokenizer(texts, tokenizer_path='models/tokenizer.json'):
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(texts)
    tokenizer_json = tokenizer.to_json()
    with open(tokenizer_path, 'w') as f:
        f.write(tokenizer_json)
    print("Tokenizer saved at", tokenizer_path)
    return tokenizer

def build_model(vocab_size, embedding_dim=16, lstm_units=32 ):
    """Builds an LSTM model with max_length consideration."""
    model = Sequential([
        Embedding(input_dim=vocab_size, output_dim=embedding_dim),
        LSTM(lstm_units),
        Dense(64, activation='relu'),
        Dropout(0.3),
        Dense(1, activation='sigmoid')
    ])
    optimizer = Adam(learning_rate=0.001)
    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
    return model


def train_and_load_model(X_train, y_train, X_val, y_val, vocab_size,  model_path="models/sentiment_model.keras"):
    """Trains the model and attempts to load it right after."""
    # Building the model
    model = build_model(vocab_size, embedding_dim=16, lstm_units=32)
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    # Setup callbacks for saving the model and early stopping
    checkpoint = ModelCheckpoint(
        'models/sentiment_model.keras',
        save_weights_only=False,
        save_best_only=True,
        verbose=1)

    early_stop = EarlyStopping(monitor='val_loss', patience=3, verbose=1)

    # Training the model
    history = model.fit(
        X_train, y_train,
        epochs=10,
        validation_data=(X_val, y_val),
        callbacks=[checkpoint, early_stop],
        verbose=1)

    # Load model weights
    print("Attempting to load the model weights...")
    try:
        model.load_weights(model_path)
        print("Model weights loaded successfully.")
    except Exception as e:
        print(f"Failed to load the model weights: {e}")
        return history, None

    return history, model

def evaluate_model(model, X_test, y_test):
    """Evaluate the given model using the test data."""
    results = model.evaluate(X_test, y_test, verbose=1)
    print(f"Test Accuracy: {results[1]*100:.2f}%")
    return results
