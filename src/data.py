import pandas as pd
from tqdm import tqdm
from kaggle.api.kaggle_api_extended import KaggleApi

tqdm.pandas()

def download_data():
    api = KaggleApi()
    api.authenticate()
    api.dataset_download_files('kazanova/sentiment140', path='./data/raw/', unzip=True)
    
def load_data(filepath):
    df = pd.read_csv(filepath, names=['target', 'ids', 'date', 'flag', 'user', 'text'], encoding='ISO-8859-1')
    print("First few rows of the dataframe:")
    print(df.head())
    print("DataFrame info:")
    print(df.info())
    return df
