import matplotlib.pyplot as plt
import os  

def plot_training_history(history, output_dir='outputs/figures', metrics=None):
    """Plot and save the training history of the model."""
    
    if metrics is None:
        metrics = ['accuracy', 'loss']   
    
    epochs = range(1, len(history.history['loss']) + 1) 
    
    plt.figure(figsize=(12, 6 * len(metrics)))

    for i, metric in enumerate(metrics):
        if metric in history.history:
            plt.subplot(len(metrics), 1, i + 1)
            plt.plot(epochs, history.history[metric], 'bo-', label=f'Training {metric.capitalize()}')
            if f'val_{metric}' in history.history:
                plt.plot(epochs, history.history[f'val_{metric}'], 'go-', label=f'Validation {metric.capitalize()}')
            plt.title(f'Training and Validation {metric.capitalize()}')
            plt.xlabel('Epoch')
            plt.ylabel(metric.capitalize())
            plt.legend()
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    # File path for saving the figure
    plot_file_path = os.path.join(output_dir, 'training_history.png')
    
    # Save the plot
    plt.savefig(plot_file_path)
    plt.close()
    print(f"Plot saved to {plot_file_path}")