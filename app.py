from flask import Flask, request, jsonify, render_template
import json
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import tokenizer_from_json

app = Flask(__name__)

try:
    model = load_model('./models/sentiment_model.keras')
    with open('models/tokenizer.json', 'r') as f:
        tokenizer_data = json.load(f)
        tokenizer = tokenizer_from_json(tokenizer_data)
    model_loaded = True
except Exception as e:
    print(f"Failed to load model or tokenizer: {e}")
    model_loaded = False

@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    if not model_loaded:
        return jsonify({'error': 'Model not loaded'}), 503
    text = request.form.get('text')
    if not text:
        return jsonify({'error': 'No text provided'}), 400
    try:
        prediction = predict_sentiment(text, tokenizer, model)
        return jsonify({'prediction': prediction})
    except Exception as e:
        return jsonify({'error': str(e)}), 500

def predict_sentiment(text, tokenizer, model, max_length=100):
    sequence = tokenizer.texts_to_sequences([text])
    if not sequence[0]:
        return "Neutral"
    padded_sequence = pad_sequences(sequence, maxlen=max_length)
    prediction = model.predict(padded_sequence)
    sentiment = 'Positive' if prediction[0][0] > 0.5 else 'Negative'
    return sentiment

if __name__ == '__main__':
    app.run(debug=True)
